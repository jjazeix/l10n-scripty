#!/bin/bash

l10nscripts=$(readlink -f $(dirname $0))

. ${l10nscripts}/find_meinproc
MEINPROC_COMMAND=`find_meinproc`
test -z "${MEINPROC_COMMAND}" && echo "No suitable version of meinproc was found. Exiting..." && exit 1
MEINPROC_COMMAND="${MEINPROC_COMMAND} --srcdir $kdoctoolsdir/src"

DOCBOOK_L10N_ALL="$kdoctoolsdir/src/customization/xsl/all-l10n.xml"
DOCBOOK_L10N_CUSTOM="$kdoctoolsdir/src/customization/xsl/kde-custom-l10n.xml"
if test -z "${DOCBOOK_L10N_ALL}" || test -z "${DOCBOOK_L10N_CUSTOM}"; then
  echo "No custom l10n files for DocBook XSLT were found. Exiting..."
  exit 1
fi

if [ ! -f subdirs ]; then
  echo "You have to run the script from the directory which contains subdirs"
  exit 1
fi


if [ "x$DOCBOOK_LOCATION" = "x" ]; then
  DOCBOOK_LOCATION=/usr/share/xml/docbook/schema/dtd/4.5/
fi

if [ "x$DOCBOOKXSL_LOCATION" = "x" ]; then
  DOCBOOKXSL_LOCATION=/usr/share/xml/docbook/stylesheet/nwalsh/
fi

if [ ! -e $kdoctoolsdir/src/customization/dtd/kdedbx45.dtd.cmake ]; then
  echo "Could not find kdedbx45.dtd.cmake make sure \$kdoctoolsdir is defined properly"
  exit 1
fi

optimized=""
if test "$1" = "--optimized"; then
        optimized="true"
        shift
fi

sed s#@DocBookXML4_DTD_DIR@#$DOCBOOK_LOCATION#g $kdoctoolsdir/src/customization/dtd/kdedbx45.dtd.cmake > $kdoctoolsdir/src/customization/dtd/kdedbx45.dtd
sed s#@DOCBOOKXSL_DIR@#$DOCBOOKXSL_LOCATION#g $kdoctoolsdir/src/customization/kde-include-common.xsl.cmake > $kdoctoolsdir/src/customization/kde-include-common.xsl
sed s#@DOCBOOKXSL_DIR@#$DOCBOOKXSL_LOCATION#g $kdoctoolsdir/src/customization/kde-include-man.xsl.cmake > $kdoctoolsdir/src/customization/kde-include-man.xsl
docbookl10nhelper $DOCBOOKXSL_LOCATION $kdoctoolsdir/src/customization/xsl/ $kdoctoolsdir/src/customization/xsl/

for lang in `cat subdirs`; do
  if [ -d $lang/docs/ ]; then
    if [ x"$optimized" = x"true" ]; then
      files=""
      dirs=`find $lang/docs/ -name *.docbook -a \( -mtime 0 -o -mtime 1 -o -mtime 2 \) -exec dirname {} \; | sort | uniq`
      for d in $dirs; do
        files="$files `find $d -name index.docbook -o -name man-*.docbook`"
      done
    else
      files=`find $lang/docs/ -name index.docbook -o -name man-*.docbook`
    fi
    
    echo $files | tr ' ' '\n' | xargs --replace=% --max-procs=`nproc` bash -c "ionice -c 2 -n 7 nice -n 30 $MEINPROC_COMMAND '%' &> /dev/null || (echo 'ERROR in %' && $MEINPROC_COMMAND '%' > /dev/null)"
  fi
done


