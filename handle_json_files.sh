#! /bin/bash
# kate: space-indent on; indent-width 2; replace-tabs on;

get_prj_name() {
  local mod=$1
  echo ${mod} | cut -d'_' -f2
}

extract_json() {
  arg1=$1; shift
  dest=$1; shift
  python3 ${dir}/createjsoncontext.py "$@" "$arg1" > json.$$.tmp
  msguniq --to-code=UTF-8 -o json.$$ json.$$.tmp 2>/dev/null
  if test -f json.$$; then
    if test ! -f  "$dest"; then
      echo "File $dest is missing!"
      mv json.$$ "$dest"
    elif diff -q -I^\"POT-Creation-Date: json.$$ "$dest" > /dev/null; then
      rm -f json.$$
      touch "$dest"
    else
      mv json.$$ "$dest"
    fi
  fi
  rm -f json.$$ json.$$.tmp
}

dir=$(dirname "$0")
env_get_paths="get_paths"
if [ -n "${SCRIPTY_I18N_BRANCH}" ]; then
    env_get_paths="${env_get_paths}.${SCRIPTY_I18N_BRANCH}"
fi
. "$dir/${env_get_paths}"
releases=$(list_modules "$dir")

for mod in $releases; do
  readarray -t -d $'\0' jsonfilelist < <(find "$BASEDIR/$(get_path "$mod")" -name "*.json" -print0 -o -name "*.json.cmake" -print0 -o -name "*.json.in" -print0 | grep -z -v '/autotests/' | grep -z -v '/tests/' | grep -z -v 'APPNAMELC' | sort -z)
  if test -n "$jsonfilelist"; then
    mod_po_path=$(get_po_path "$mod")
    prj_name=$(get_prj_name "$mod")

    repo_dir="$BASEDIR/$(get_path "$mod")"

    extract_json $repo_dir "templates/messages/${mod_po_path}/${prj_name}._json_.pot" "${jsonfilelist[@]}"
    parallel python3 ${dir}/filljsonfrompo.py $repo_dir "{}" "$L10NDIR" "${mod_po_path}" "${prj_name}._json_.po" ::: "${jsonfilelist[@]}"
  fi
done
