KDE_PROJECTS_API_URL="https://projects.kde.org/api/v1"

I18N_BRANCH='trunkKF6'

function check_kde_projects_requirements
{
  local programs_required="curl jq"
  for program_checked in ${programs_required}; do
    ${program_checked} --help &>/dev/null
    if [ $? -ne 0 ]; then
      printf "\'${program_checked}\' is required but it was not found\n" 1>&2
      exit 1
    fi
  done
}

function list_modules
{
  check_kde_projects_requirements

  # --retry 5 is a safeguard
  local all_modules=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifiers?active=true | jq -r '.[]' 2>/dev/null)
  if [ $? -ne 0 ]; then
    all_modules=""
  fi
  modules=""
  # most of the code which follows should and wll be replaced
  # by proper filtering on the API side
  for M in ${all_modules}; do
    local project_details=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifier/${M} 2>/dev/null)
    local found_repo=$(echo "${project_details}" | jq -r '.repo' 2>/dev/null)

    [[ "${found_repo}" =~ (unmaintained|historical|sysadmin)/ ]] && continue

    local found_branch=$(echo "${project_details}" | jq -r ".i18n.${I18N_BRANCH}" 2>/dev/null)
    if [ -z "${found_branch}" ] || [ "${found_branch}" = "null" ]; then
      found_branch="none"
    fi

    expected_branch=$(get_branch ${M})
    if [ -z "${expected_branch}" ] || [ "${expected_branch}" = "get_branch_none" ]; then
      expected_branch="none"
    fi
    if [ "${found_branch}" != "${expected_branch}" ]; then
      printf "Warning: '%s' has different branches in get_paths (%s) than in XML file (%s)\n" ${M} ${expected_branch} ${found_branch} >&2
    fi
    if [ "${expected_branch}" != "none" ]; then
      modules="${modules} ${M}"
    fi
  done
  echo $modules
}

function get_path
{
	case "$1" in
		l10n)
			echo trunk/l10n-kf6
			;;
		*)
                        echo git-unstable-kf6/$1
			;;
	esac
}

function get_po_path
{
	echo $1
}

function get_vcs
{
	case "$1" in
		l10n)
			echo svn
			;;
		*)
			echo git
			;;
	esac
}


function get_branch
{
	case "$1" in
		attica|baloo|bluez-qt|breeze-icons|extra-cmake-modules|frameworkintegration|kapidox|karchive|kauth|kbookmarks|kcalendarcore|kcmutils|kcodecs|kcompletion|kconfig|kconfigwidgets|kcontacts|kcoreaddons|kcrash|kdbusaddons|kdav|kdeclarative|kded|kdesu|kdnssd|kdoctools|kfilemetadata|kglobalaccel|kguiaddons|kholidays|ki18n|kiconthemes|kidletime|kimageformats|kio|kirigami|kitemmodels|kitemviews|kjobwidgets|knewstuff|knotifications|knotifyconfig|kpackage|kparts|kpeople|kplotting|kpty|kquickcharts|krunner|kservice|ktexteditor|ktexttemplate|ktextwidgets|kunitconversion|kwallet|kwidgetsaddons|kwindowsystem|kxmlgui|modemmanager-qt|networkmanager-qt|prison|purpose|qqc2-desktop-style|solid|sonnet|syndication|syntax-highlighting|threadweaver|kcolorscheme|kstatusnotifieritem|kuserfeedback)
			# Frameworks
			echo "master"
			;;
		kdialog|tokodon|ktrip|koko|kate|konsole|kpmcore|partitionmanager|falkon|kaccounts-integration|kaccounts-providers|kdenetwork-filesharing|konqueror|ktorrent|libktorrent|neochat|spectacle|elisa|kcron|ksystemlog|kcalc|kcharselect|kdebugsettings|ktimer|kwalletmanager|sweeper|kamera|kcolorchooser|kruler|audiocd-kio|libkcompactdisc|kbackup|kdf|markdownpart|kmag|kmousetool|kmouth|blinken|kbruch|klettres|kturtle|svgpart|filelight|kteatime|keditbookmarks|kfind|akonadi|akonadi-calendar|akonadi-calendar-tools|akonadiconsole|akonadi-contacts|akonadi-import-wizard|akonadi-mime|akonadi-notes|akonadi-search|akregator|calendarsupport|eventviews|grantlee-editor|grantleetheme|incidenceeditor|itinerary|kaddressbook|kalarm|kcalutils|kdepim-addons|kdepim-runtime|kidentitymanagement|kimap|kitinerary|kldap|kleopatra|kmail|kmail-account-wizard|kmailtransport|kmbox|kmime|knotes|kontact|kontactinterface|korganizer|kpimtextedit|kpkpass|ksmtp|ktnef|libgravatar|libkdepim|libkgapi|libkleo|libksieve|mailcommon|mailimporter|mbox-importer|merkuro|messagelib|pimcommon|pim-data-exporter|pim-sieve-editor|zanshin|angelfish|arianna|bomber|bovo|granatier|kajongg|kapman|katomic|kblackbox|kblocks|kbounce|kbreakout|kdiamond|kfourinline|kgoldrunner|kigo|killbots|kiriki|kjumpingcube|klickety|klines|kmahjongg|kmines|knavalbattle|knetwalk|knights|kolf|kollision|konquest|kpat|kreversi|kshisen|ksirk|ksnakeduel|kspaceduel|ksquares|ksudoku|ktuberling|kubrick|libkdegames|libkmahjongg|lskat|palapeli|picmi|kweather|analitza|kalgebra|kanagram|kgeography|khangman|kiten|kwordquiz|parley|kontrast|khelpcenter|yakuake|kalk|plasmatube|kcachegrind|kongress|dragon|qmlkonsole|kjournald|kapptemplate|dolphin|dolphin-plugins|kclock|mimetreeparser|kio-extras|alligator|audiotube|kasts|keysmith)
			# Gear
			echo "master"
			;;
		bluedevil|breeze|breeze-grub|breeze-gtk|breeze-plymouth|discover|drkonqi|kactivitymanagerd|kde-cli-tools|kdecoration|kde-gtk-config|kdeplasma-addons|kgamma|kglobalacceld|kinfocenter|kmenuedit|kpipewire|kscreen|kscreenlocker|ksshaskpass|ksystemstats|kwallet-pam|kwayland|kwayland-integration|kwin|kwrited|libkscreen|libksysguard|milou|oxygen|plasma-browser-integration|plasma-desktop|plasma-disks|plasma-integration|plasma-mobile|plasma-nano|plasma-nm|plasma-pa|plasma-sdk|plasma-systemmonitor|plasma-tests|plasma-thunderbolt|plasma-vault|plasma-workspace|plasma-workspace-wallpapers|plymouth-kcm|polkit-kde-agent-1|powerdevil|sddm-kcm|systemsettings|xdg-desktop-portal-kde|qqc2-breeze-style|plasma-firewall|layer-shell-qt|oxygen-sounds|flatpak-kcm|plasma-welcome|plasma5support|krdp|print-manager|libplasma|plasma-activities|plasma-activities-stats|ocean-sound-theme)
			# Plasma
			echo "master"
			;;
		oxygen-icons|atlantik|fielding|hash-o-matic|plasma-phonebook|toad|qrca|kirigami-addons|kunifiedpush|haruna|accessibility-inspector|kdiagram|kweathercore|ktimetracker)
			# independent release
			echo "master"
			;;
		spacebar)
			# plasma-mobile
			echo "master"
			;;
		rolisteam|rolisteam-community-data|rolisteam-diceparser|rolisteam-packaging)
			echo "master"
			;;
		kmoretools|ksvg)
			# Other
			echo "master"
			;;
		*)
			echo "get_branch_none"
			;;
	esac
}

function get_repo_name
{
	echo $(get_po_path $1)
}

function get_full_repo_path
{
	check_kde_projects_requirements

	local repo_full_path=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifier/$1 | jq -r '.repo' 2>/dev/null)
	res=$?

	if [ ${res} -eq 0 ] && [ -n "${repo_full_path}" ]; then
		echo "${repo_full_path}"
	else
		echo "ERROR: url not found for $1"
		exit 1
	fi
}

function get_url
{
	if [ -n "$1" ]; then
		echo "kde:$(get_full_repo_path $1).git"
	fi
}
