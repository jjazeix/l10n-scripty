KDE_PROJECTS_API_URL="https://projects.kde.org/api/v1"

I18N_BRANCH='trunkKF5'

function check_kde_projects_requirements
{
  local programs_required="curl jq"
  for program_checked in ${programs_required}; do
    ${program_checked} --help &>/dev/null
    if [ $? -ne 0 ]; then
      printf "\'${program_checked}\' is required but it was not found\n" 1>&2
      exit 1
    fi
  done
}

function list_modules
{
  check_kde_projects_requirements

  # --retry 5 is a safeguard
  local all_modules=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifiers?active=true | jq -r '.[]' 2>/dev/null)
  if [ $? -ne 0 ]; then
    all_modules=""
  fi
  modules=""
  # most of the code which follows should and wll be replaced
  # by proper filtering on the API side
  for M in ${all_modules}; do
    local project_details=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifier/${M} 2>/dev/null)
    local found_repo=$(echo "${project_details}" | jq -r '.repo' 2>/dev/null)

    [[ "${found_repo}" =~ (unmaintained|historical|sysadmin)/ ]] && continue

    local found_branch=$(echo "${project_details}" | jq -r ".i18n.${I18N_BRANCH}" 2>/dev/null)
    if [ -z "${found_branch}" ] || [ "${found_branch}" = "null" ]; then
      found_branch="none"
    fi

    expected_branch=$(get_branch ${M})
    if [ -z "${expected_branch}" ] || [ "${expected_branch}" = "get_branch_none" ]; then
      expected_branch="none"
    fi
    if [ "${found_branch}" != "${expected_branch}" ]; then
      printf "Warning: '%s' has different branches in get_paths (%s) than in XML file (%s)\n" ${M} ${expected_branch} ${found_branch} >&2
    fi
    if [ "${expected_branch}" != "none" ]; then
      modules="${modules} ${M}"
    fi
  done
  echo $modules
}

function get_path
{
	case "$1" in
		l10n)
			echo trunk/l10n-kf5
			;;
		*)
                        echo git-unstable-kf5/$1
			;;
	esac
}

function get_po_path
{
	echo $1
}

function get_vcs
{
	case "$1" in
		l10n)
			echo svn
			;;
		*)
			echo git
			;;
	esac
}


function get_branch
{
	case "$1" in
		websites-docs-krita-org)
			# yes, it's stable, but it's where the site is generated from
			echo "krita/5.2"
			;;
		neon-neon-repositories)
			echo "Neon/unstable"
			;;
		phonon|phonon-gstreamer|phonon-vlc)
			echo "master"
			;;
		attica|baloo|bluez-qt|breeze-icons|extra-cmake-modules|frameworkintegration|kapidox|karchive|kauth|kbookmarks|kcalendarcore|kcmutils|kcodecs|kcompletion|kconfig|kconfigwidgets|kcontacts|kcoreaddons|kcrash|kdbusaddons|kdav|kdeclarative|kded|kdelibs4support|kdesignerplugin|kdesu|kdewebkit|kdnssd|kdoctools|kemoticons|kfilemetadata|kglobalaccel|kguiaddons|kholidays|khtml|ki18n|kiconthemes|kidletime|kimageformats|kinit|kio|kirigami|kitemmodels|kitemviews|kjobwidgets|kjs|kjsembed|kmediaplayer|knewstuff|knotifications|knotifyconfig|kpackage|kparts|kpeople|kplotting|kpty|kquickcharts|kross|krunner|kservice|ktexteditor|ktextwidgets|kunitconversion|kwallet|kwayland|kwidgetsaddons|kwindowsystem|kxmlgui|kxmlrpcclient|modemmanager-qt|networkmanager-qt|prison|purpose|qqc2-desktop-style|solid|sonnet|syndication|syntax-highlighting|threadweaver|libplasma|plasma-activities|plasma-activities-stats)
			# Frameworks
			echo "kf5"
			;;
		artikulate|cantor|kalzium|kdeedu-data|kig|kmplot|kqtquickcharts|ktouch|libkeduvocdocument|marble|minuet|rocs|step|gwenview|kdegraphics-mobipocket|kdegraphics-thumbnailers|kolourpaint|libkdcraw|libkexiv2|libkipi|libksane|okular|ark|kgpg|ffmpegthumbs|juk|k3b|kdenlive|kmix|kwave|libkcddb|cervisia|kde-dev-utils|kde-dev-scripts|kdesdk-kio|kdesdk-thumbnailers|kompare|libkomparediff2|lokalize|poxml|umbrello|kget|kio-gdrive|kio-zeroconf|konversation|krdc|krfb|signon-kwallet-extension|kross-interpreters|baloo-widgets|kosmindoormap|kpublictransport|kimagemapeditor|kamoso|kirigami-gallery|kipi-plugins|kdeconnect-kde|kdevelop|kdev-php|kdev-python|skanpage|ksanecore)
			# Gear (formerly Release Service)
			echo "master"
 			;;
		kio-extras)
			# needed for cross-kf5/kf6 compatibility
			echo "kf5"
			;;
		kleopatra|libkleo|mimetreeparser)
			# GnuPG Desktop Gear (see https://invent.kde.org/sysadmin/repo-metadata/-/merge_requests/209/)
			echo "kf5"
			;;
		plasma-wayland-protocols)
			echo "master"
			;;
		calligra|calligraplan|kexi)
			echo "master"
			;;
		akonadiclient|akonadi-phabricator-resource|alkimia|amarok|amor|apper|atcore|atelier|audex|basket|choqok|clazy|colord-kde|cutehmi|digikam|digikam-doc|distro-release-notifier|doxyqml|elf-dissector|gcompris|gcompris-data|git-lab|heaptrack|ikona|isoimagewriter|kaffeine|kaidan|kairo|kalternatives|kasync|kbibtex|kbibtex-testset|kcgroups|kcm-grub2|kdav2|kdb|kdeconnect-android|kdeconnect-ios|kdesrc-build|kdesvn|kdev-css|kdevelop-pg-qt|kdev-embedded|kdev-executebrowser|kdev-krazy2|kdev-mercurial|kdev-ruby|kdev-upload|kdev-valgrind|kdev-verapp|kdev-xdebug|kdiff3|keurocalc|kgraphviewer|khipu|kid3|kije|kile|kimap2|kio-fuse|kio-gopher|kio-stash|kirogi|kjots|klimbgrades|kmarkdownwebview|kmplayer|kmuddy|kmymoney|kodaskanna|kolorfill|kolor-manager|kooka|kookbook|kpeoplesink|kpeoplevcard|kphotoalbum|kproperty|kquickimageeditor|kquickitemviews|kregexpeditor|krename|kreport|krita|kseexpr|kronometer|krusader|skladnik|kstars|kst-plot|ktechlab|kube|kubuntu-debug-installer|kubuntu-driver-kcm|kubuntu-notification-helper|kuickshow|kup|kwebkitpart|kwindowsaddons|kxstitch|labplot|libdebconf-kde|libkimageannotator|libkvkontakte|libmediawiki|libqaccessibilityclient|libqapt|libtmdbqt|liquidshell|mangonel|mark|massif-visualizer|muon|okteta|peruse|mycroft-plasmoid|plasma-pk-updates|pokipoki|polkit-qt-1|pulseaudio-qt|pvfviewer|qca|qtcurve|qtjolie|ring-kde|rkward|rsibreak|ruqola|rust-qt-binding-generator|samba-mounter|kdesdk-devenv-dependencies|sink|skanlite|skrooge|smb4k|snorenotify|subtitlecomposer|symboleditor|symmy|systemdgenie|tellico|trojita|ubiquity-slideshow-neon|upnp-lib-qt|wacomtablet|washipad|whoopsie-kcm|xdg-portal-test-kde|kgeotag|kopeninghours|totalreqall|drkonqi-pk-debug-installer|arkade|kio-s3|kde-nomodeset|pikasso|docker-neon|stopmotion|libqmycroft|khealthcertificate|vakzination|kquickchatcomponents|croutons|kiss|qml-lsp|nongurigaeru|calamares-bigscreen-branding|kde-inotify-survey|telly-skout|eloquens|melon|francis|licentia|kommit|pico-wizard|kdsoap-ws-discovery-client|raven|ghostwriter|kdenlive-opentimelineio|kio-admin|ktextaddons|vail|perceptualcolor|futuresql|xwaylandvideobridge|marknote|rattlesnake|klevernotes|ktp-accounts-kcm|ktp-approver|ktp-auth-handler|ktp-call-ui|ktp-common-internals|ktp-contact-list|ktp-contact-runner|ktp-desktop-applets|ktp-filetransfer-handler|ktp-kded-module|ktp-send-file|ktp-text-ui|kfloppy|mpvqt|kalm)
			# independent release
			echo "master"
			;;
		tok)
			# independent release (more)
			echo "dev"
			;;
		calindori|kaccounts-mobile|krecorder|mtp-server|plasma-camera|plasma-dialer|plasma-maliit-framework|plasma-mycroft-mobile|plasma-phone-settings|plasma-samegame|plasma-settings|trainer|daykountdown|plasma-mobile-sounds)
			# plasma-mobile
			echo "master"
			;;
		aura-browser|bigscreen-application-launcher-skill|bigscreen-debos-image-rpi4|bigscreen-platform-skill|mycroft-bigscreen-setup|mycroft-ptt-client|mycroft-skill-installer|peertube-voice-application|plank-player|soundcloud-voice-application|wikidata-voice-application|youtube-voice-application|bigscreen-image-settings|plasma-remotecontrollers)
			# plasma-bigscreen
			echo "master"
			;;
		lancelot|latte-dock|plasma-active-window-control|plasma-bigscreen|plasma-pass|plasma-redshift-control|plasma-simplemenu|smaragd)
			# plasma namespace but not Plasma
			echo "master"
			;;
		buho|index-fm|maui-accounts-dbus-daemon|maui-accounts-gui|maui-clip|maui-communicator|mauikit|maui-libdavclient|maui-pix|maui-shelf|maui-station|nomad-style|nota|vvave|maui-booth|mauikit-filebrowsing|mauikit-imagetools|mauikit-texteditor|mauikit-accounts|maui-strike|bonsai|mauiman|mauikit-calendar|mauikit-documents|maui-fiery|mauikit-terminal)
			# mauikit
			echo "master"
			;;
		websites-akademy-kde-org|websites-calligra-org|websites-capacity|websites-eco-kde-org|websites-elisa-kde-org|websites-gcompris-net|websites-ghostwriter-kde-org|websites-hugo-kde|websites-kate-editor-org|websites-kontact-kde-org|websites-krita-org|websites-kstars-kde-org|websites-manifesto-kde-org|websites-okular-kde-org|websites-planet-kde-org|websites-skrooge-org|websites-kde-org|websites-apps-kde-org|websites-timeline-kde-org|websites-plasma-mobile-org|websites-plasma-bigscreen-org|websites-25years-kde-org|documentation-develop-kde-org|documentation-docs-kdenlive-org|websites-kdevelop-org|websites-video-subtitles)
			echo "master"
			;;
		*)
			echo "get_branch_none"
			;;
	esac
}

function get_repo_name
{
	echo $(get_po_path $1)
}

function get_full_repo_path
{
	check_kde_projects_requirements

	local repo_full_path=$(curl --retry 5 -s ${KDE_PROJECTS_API_URL}/identifier/$1 | jq -r '.repo' 2>/dev/null)
	res=$?

	if [ ${res} -eq 0 ] && [ -n "${repo_full_path}" ]; then
		echo "${repo_full_path}"
	else
		echo "ERROR: url not found for $1"
		exit 1
	fi
}

function get_url
{
	if [ -n "$1" ]; then
		echo "kde:$(get_full_repo_path $1).git"
	fi 
}
